package com.alu4r.springbootcrud.Handler;

import com.alu4r.springbootcrud.exception.UserNotExistException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义异常处理器,把异常信息改成我们定制的
 * 出现错误的时候会被BasicErrorController处理
 */
@ControllerAdvice
public class MyExceptionHandler {
    /**
     * 1.浏览器器客户端返回的都是json
     * @param e
     * @return
     */

//    @ResponseBody
//    @ExceptionHandler(UserNotExistException.class)
//    public Map<String,Object> handlerException(Exception e){
//        Map<String,Object> map = new HashMap<>();
//        map.put("code","user.notExist");
//        map.put("message",e.getMessage());
//        return map;
//    }

    /**
     * 2.浏览器器返回text/html,客户端返回json
     * 设置状态码，客户端就可以直接去响应的页面了，如果是客户端就会自动转成json直接转发一下
     * 但是无法把定制的数据携带到前台
     * @param e
     * @return
     */

//    @ExceptionHandler(UserNotExistException.class)
//    public String handlerException(Exception e, HttpServletRequest request){
//        Map<String,Object> map = new HashMap<>();
//        map.put("code","user.notExist");
//        map.put("message","用户出错了");
//        //传入自己的状态码，默认是200
//        request.setAttribute("javax.servlet.error.status_code",500);
//        return "forward:/error";
//    }


    /**
     * 3.推荐方式
     * 配合MyErrorAttributes类使用
     *
     * @param e
     * @return
     */

    @ExceptionHandler(UserNotExistException.class)
    public String handlerException(Exception e, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", "user.notExist");
        map.put("message", "用户出错了");
        //传入自己的状态码，默认是200
        request.setAttribute("javax.servlet.error.status_code", 500);
        //将这些数据进去，MyErrorAttributes方便来取用
        request.setAttribute("ext", map);
        return "forward:/error";
    }
}
