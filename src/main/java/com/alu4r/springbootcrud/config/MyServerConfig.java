package com.alu4r.springbootcrud.config;

import com.alu4r.springbootcrud.filter.MyFilter;
import com.alu4r.springbootcrud.listener.MyListener;
import com.alu4r.springbootcrud.servlet.MyServlet;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import javax.servlet.Filter;
import javax.servlet.Servlet;
import java.util.Arrays;

@Configuration
public class MyServerConfig {


    //启动了listener
    @Bean
    public ServletListenerRegistrationBean servletListenerRegistrationBean() {
        return new ServletListenerRegistrationBean(new MyListener());
    }

    @Bean
    //注册filter
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean<Filter> filterFilterRegistrationBean = new FilterRegistrationBean<>();
        filterFilterRegistrationBean.setFilter(new MyFilter());
        filterFilterRegistrationBean.setUrlPatterns(Arrays.asList("/abc"));
        return filterFilterRegistrationBean;
    }


    //注册我们自己写的servlet   MyServlet
    @Bean
    public ServletRegistrationBean myServlet() {
        ServletRegistrationBean servlet = new ServletRegistrationBean(new MyServlet(), "/abc");
        return servlet;
    }

    //注册嵌入式的servlet容器
//    public EmbeddedServletContainerCustomizer embeddedServletContainerCustomizer(){
//        return new EmbeddedServletContainerCustomizer(){
//
//        }
//    }
}
