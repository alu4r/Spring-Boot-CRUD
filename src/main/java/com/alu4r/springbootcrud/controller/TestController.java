package com.alu4r.springbootcrud.controller;

import com.alu4r.springbootcrud.exception.UserNotExistException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class TestController {

    @RequestMapping("/test")
    public String test() {
        return "success";
    }

    @ResponseBody
    @RequestMapping("/hello")
    public String user(@RequestParam("user") String user) {
        if (user.equals("admin")) {
            throw new UserNotExistException();
        }
        return "Hello World";
    }
}
